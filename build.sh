#!/bin/bash
self_path=`cd $(dirname $0);pwd`
export GOBIN=$self_path/resource
go build $self_path/main.go
go install
rm -rf $self_path/main
cp -arf $self_path/.env $self_path/resource/.env
docker build -t transfer-core $self_path
