FROM ubuntu:18.04

WORKDIR /app

ADD resource/* /app/

ENTRYPOINT [ "/app/entry.sh" ]
